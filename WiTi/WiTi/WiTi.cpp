#include "stdafx.h"
#include <iostream>
#include <algorithm>
#include <string>
#include <fstream>
#include <vector>
#include <Windows.h>
#include <math.h>

using namespace std;

class dane
{
public:
	int p;
	int w;
	int d;


	dane(int lew, int hiena, int gepard);
	dane();
	dane &operator= (dane x);
	bool operator<(const dane x) const;
	bool operator>(const dane x) const;

};

dane::dane(int lew, int hiena, int gepard) {
	p = lew;
	w = hiena;
	d = gepard;
}
dane::dane() {
	p = 0;
	w = 0;
	d = 0;
}
dane &dane::operator=(dane x) {
	p = x.p;
	w = x.w;
	d = x.d;
	return *this;


}

bool dane::operator<(const dane x) const {
	return(p > x.p);
}

bool dane::operator>(const dane x) const {
	return(d < x.d);
}

LARGE_INTEGER startTimer()
{
	LARGE_INTEGER start;
	DWORD_PTR oldmask = SetThreadAffinityMask(GetCurrentThread(), 0);
	QueryPerformanceCounter(&start);
	SetThreadAffinityMask(GetCurrentThread(), oldmask);
	return start;
}

LARGE_INTEGER endTimer()
{
	LARGE_INTEGER stop;
	DWORD_PTR oldmask = SetThreadAffinityMask(GetCurrentThread(), 0);
	QueryPerformanceCounter(&stop);
	SetThreadAffinityMask(GetCurrentThread(), oldmask);
	return stop;
}

int Cmax(vector <dane > vec, int rozmiar)
{
	int Cmax = 0;
	int Smax = 0;
	int temp = 0;
	int i = 0;
	while (i < rozmiar)
	{
		if (Smax < vec[i].p)
		{
			temp = vec[i].p - Smax;
			Smax = Smax + temp;
		}
		/*
		else
		{
		Smax = Smax + vec[i][1];
		}*/
		Smax = Smax + vec[i].w;

		if (Smax + vec[i].d > Cmax)
			Cmax = Smax + vec[i].d;
		i++;
	}
	return Cmax;
}

void wyswietl(vector <dane > vec, int rozmiar, int ilosc)
{
	for (int i = 0; i < rozmiar; i++)
	{
		cout << vec[i].p << " ";
		cout << vec[i].w << " ";
		cout << vec[i].d << " ";
		cout << endl;
	}
}

vector<int> dectobin( int vec) {
	int pom = vec, temp = vec, i = 0,j=1,rozmiar=1,pomoc=0;
	vector<int> tab;
	if (vec > 2) {
		while (vec > rozmiar)
		{
			rozmiar = pow(2, j);
			j++;
		}
		if(vec!=rozmiar)
			j--;
		while (i < j) {
			temp = pom % 2;
			tab.push_back(temp);
			pom = pom / 2;
			//cout << tab[i] << endl;
			i++;
		}
	}
	else
	{
		if (vec == 1) {
			tab.push_back(1);
			//cout << tab[0] << endl;
		}
		if (vec == 2) {
			tab.push_back(0);
			tab.push_back(1);
			//cout << tab[0] << endl;
			//cout << tab[1] << endl;
		}
	}
	return tab;
}

int bintodec(vector<int> vec) {
	int wynik = 0, i = 0;
	while (i<vec.size()) {
		if (vec[i] == 0) {
			//wynik=wynik+pow(2,i);
		}
		else
			wynik = wynik + pow(2, i);
		i++;
	}
	//cout << wynik;
	return wynik;
}

vector<dane> GetTask(vector<int> vec, vector<dane> dec)
{
	int i = 0;
	vector<dane> tab;
	while(i<vec.size())
	{
		if (vec[i] == 1)
			tab.push_back(dec[i]);
		i++;
	}
	return tab;
}

vector<int> WitiNum(vector<int> vec, vector<int> max)
{
	int i = 0, cos;
	vector<int> temp=vec, wynik;
	while (i < vec.size())
	{
		temp = vec;
		if (temp[i] == 1) 
		{
			temp[i] = 0;
			cos = bintodec(temp);
			wynik.push_back(max[cos - 1]);
		}
		i++;
	}
	return wynik;
}

int minWiTi(vector<dane> vec, vector<int> max)
{
	int a, b,wynik=10000,i=0,suma=0,j= vec.size() - 1;
	while (i < vec.size())
	{
		suma += vec[i].p;
		i++;
	}
	i = 0;
	while (i < vec.size())
	{
		wynik = min(a = max(suma - vec[i].d, 0)*vec[i].w + max[i], wynik);
		i++;
	}
	return wynik;
}

int minW(int ilosc, vector<dane> vec)
{
	int wynik = 0, suma = 0, j = 0,i=1,binar=0;
	double taxi = pow(2, ilosc)+1;
	vector<int> maxwiti, pomoc, wartWiti;
	vector<dane> temp;
	while (i < taxi)
	{
			pomoc = dectobin(i);
			temp = GetTask(pomoc, vec);
			if (temp.size() == 1)
			{
				maxwiti.push_back(max(temp[0].p - temp[0].d, 0)*temp[0].w);
			}
			else
			{
				wartWiti = WitiNum(pomoc, maxwiti);
				maxwiti.push_back(minWiTi(temp, wartWiti));
			}
			i++;
			if (i == taxi - 1)
				break;
	}
	cout << maxwiti.back() << "<- wynik" << endl;
	return 0;





	/*vector< vector <dane > > tab1(ilosc, vector<dane>(ilosc));
	if (ilosc == 1)
	{
		wynik = max(vec[0].p - vec[0].d, 0)*vec[0].w;
		//cout<<"wynik "<<wynik<<endl;
	}
	else
	{
		for (int i = 0; i < ilosc; i++)    // rozbijanie listy zadan na wektory z konkretnymi przypadkami
		{
			int k = 0, l = 0;
			while (k<ilosc)
			{
				if (k != i)
				{
					tab1[i][l] = dane(vec[k].p, vec[k].w, vec[k].d);
					//cout << "Wyswietlam zadanie " << tab1[i][l].p << endl;
					l++;
					k++;
				}
				else
					k++;
			}
		}
		wynik = 10000;
		for (int i = 0; i <ilosc; i++)
		{
			suma += vec[i].p; //suma danego przypadku
		}
		for (int i = 0; i < ilosc; i++)
		{
			maxwiti[i] = max(suma - vec[i].d, 0)*vec[i].w + minW(ilosc - 1, j, tab1[i]);
			//cout<<"wynik "<<maxwiti[i]<<" ilosc "<<ilosc<<" "<<vec[i].p<<endl;
		}
		for (int i = 0; i < ilosc; i++)
		{
			wynik = min(maxwiti[i], wynik);
		}
		stala += wynik;
	}
	return wynik;*/
}

int main()
{
	int i = 0, rozmiar, p, w, d;
	vector<int> tab;
	for(int j=0;j<14;j++)
	tab.push_back(j);
	fstream plik;
	string cos;
	LARGE_INTEGER czas_s, czas_st, wynik;
	__int64 freq;
	QueryPerformanceFrequency((LARGE_INTEGER *)&freq);
	cout << "Podaj naze pliku: ";
	cin >> cos;
	cout << endl;
	plik.open("data15.txt", ios::in);
	plik >> rozmiar;
	vector <dane> vec(rozmiar);
	vector < dane > vec1(rozmiar);
	while (i<rozmiar)
	{
		plik >> p;
		plik >> w;
		plik >> d;
		vec[i] = dane(p, w, d);

		i++;
	}
	minW(rozmiar, vec);
	//bintodec(tab);
	//dectobin(8);
	//cout << stala;//Witi(vec, rozmiar);
	//return 0;
}